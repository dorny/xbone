import deps = require('./deps')
var when = deps.when

var vendor = [ '','-webkit-', '-moz-', '-ms-', '-o-']
var re_unit = /^([0-9.]+)(.*)$/


export function get(el, prop) {
	return window.getComputedStyle(el)[prop]
}

export function set(el, props, value?) {
	switch (typeof props) {
		case 'string':
			el.style.setProperty(props, value)
			break

		case 'object': {
			for(var key in props)
				el.style.setProperty(key, props[key])
			break
		}

		default:
			throw new TypeError()
	}
}

export function clear(el, props?) {
	if (props)
		props.forEach( (p)=> el.style.removeProperty(p))
	else
		el.removeAttribute('style')
}

export function compute(el, prop: string, op: string, value: number) {
	var match, curr, unit

	if ((match = get(el, prop).match(re_unit))==null)
		throw new TypeError("Invalid property")

	curr = parseFloat(match[0])
	unit = match[1]

	switch (op) {
		case '+': return (curr + value) + unit
		case '-': return (curr - value) + unit
		case '*': return (curr * value) + unit
		case '/': return (curr / value) + unit
	}
}

export function transit(el, props, duration: number = 400, easing='ease', delay=0) {
	var current = window.getComputedStyle(el)
	  , defer = when.defer()
	  , vals = {}

	for(var key in props) {

		if (!el.style.hasOwnProperty(key))
			el.style.setProperty(key, current.getPropertyValue(key) || '0')

		if (props[key]=='auto') {
			var old = el.style.getPropertyValue(key)
			el.style.setProperty( key, 'auto')
			vals[key] = current.getPropertyValue(key)
			el.style.setProperty(key, old)
		}
		else
			vals[key] = props[key]
	}

	_transition(el, Object.keys(props), duration, easing, delay)

	for(var key in vals)
		el.style.setProperty( key, vals[key])

	setTimeout(()=> defer.resolve(el), duration+delay)
	return defer.promise
}



function _transition(el, properties: string[], duration: number, timing: string, delay: number) {
	var val = []
	properties.forEach( (prop)=> val.push(prop + ' ' + duration+'ms '+ timing + ' ' + delay+'ms'))
	vendor.forEach( (v)=> {
		el.style.setProperty( v+'transition', val.join(','))
	})
}
