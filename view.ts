import deps = require('./deps')
import Model = require('./model')

var _ = deps.lodash
var $ = deps.zepto
var when = deps.when
var slice = Array.prototype.slice

export = View



var VIEW_CLASSES = {}
var VIEW_INSTANCES = {}
var ACTIVE_VIEW = null
var NAVIGATION_ACTIVE = false

var DATA_BINDING_TYPES = ['attr','class','enabled','html','show','text','value','visible']
var EVENT_BINDING_TYPES = ['on', 'value', 'selected']
var BINDING_TYPES = _.union(DATA_BINDING_TYPES, EVENT_BINDING_TYPES)


class View extends Model {

	// Main attriubtes
	public id: string;
	public el: HTMLElement;
	public $el: ZeptoCollection;
	public parent: View;
	public routes: { [path: string]: (...paths:string[]) => void };
	public options;

	// Child Views
	public childs: { [id: string]: View} = {};
	static childViews: { [id:string]: typeof View};

	// Bindings container
	private self: View;
	private bindings : {
		[id: string]: {
			view: View;
			modelName: string;
			list: {
				el: HTMLElement;
				type: string;
				key: string;
				not: boolean;
				largs: string;
				rargs: string;
			}[];
		}
	};

	// Formats converters
	public formats = {};

	// Class default options
	static options : {
		parse?: boolean;
		tabs?: boolean;
		navs?: boolean;
		navsSelector?: string;
		navsEvent?: string;
		[key: string]: any;
	} = { parse: true, navs: true, tabs: false, navsSelector: 'a[data-navigate],button[data-navigate]', navsEvent: 'click'};


	// Constructor
	// -----------------------------------------------------

	constructor( options: {
		el: HTMLElement;
		parent?: View;
		[key:string]: any;
	})
	{
		super()

		var el = options.el
		var parent = options.parent
		delete options.el
		delete options.parent

		if (! (el instanceof HTMLElement))
			throw new TypeError('"el" is not instance of HTMLElement')

		// Element & Parent
		this.el = el
		this.$el = $(el)
		this.el['__XBONE_VIEW'] = true
		this.parent = parent
		this.bindings = {}
		this.routes = {}
		this.self = this

		// Options
		var elemOptions
		if (el.hasAttribute('data-view-options'))
			elemOptions = this.parseDataAttr( el.getAttribute('data-view-options'))

		var opts = _.merge( {}, View.options, this.constructor.options, elemOptions, options)
		this.options = opts

		// ID & global|element registration
		this.id = (opts.id != null) ? opts.id : (this.el.id || _.uniqueId('view'))

		if (VIEW_INSTANCES[this.id])
			throw new Error('View with id "'+this.id+'" already exists')

		VIEW_INSTANCES[this.id] = this

		// Parent
		if (parent)
			parent.addChild(this)

		// Model
		if (opts.set)
			this.set(opts.set)

		this.set('shown', !this.el.hasAttribute('hidden'))
		this.on('change', ()=> this.emit( 'change:self', this, false))

		if (opts.navs)
			this.bindNavigation()

		if (this.constructor.childViews) {
			var classes = this.constructor.childViews
			for(var key in classes)
				this.all(key).forEach( (el)=>this.parseDataView( el, classes[key]) )
		}

		if (opts.parse) {
			_.defer(()=> {
				this.parseDOM()
				this.emit('parsed')
				this.refreshBindings()
			})
		}
	}



	// Initializer
	// -----------------------------------------------------

	/*protected*/ init(opts?) { /* Override me */}



	// Shortcuts
	// ----------------------------------------------------

	public getEl(id: string) { return <HTMLElement> document.getElementById(id) }
	public one(selector: string, el?) { return <HTMLElement> (el || this.el).querySelector(selector) }
	public all(selector: string, el?) { return <HTMLElement[]> slice.call( (el || this.el).querySelectorAll(selector)) }
	public byTag(name: string, el?) { return <HTMLElement[]> slice.call( (el || this.el).getElementsByTagName(name)) }
	public byClass(name: string, el?){ return <HTMLElement[]> slice.call( (el || this.el).getElementsByClassName(name)) }

	public showEl(el: Element) { el.removeAttribute('hidden') }
	public hideEl(el: Element) { el.setAttribute('hidden','') }

	public create(tagName, attrs?, childs?) : HTMLElement {
		var el = document.createElement(tagName)

		if (attrs) {
			if ('text' in attrs) {
				el.textContent = attrs.text
				delete attrs.text
			}

			if ('html' in attrs) {
				el.innerHTML = attrs.html
				delete attrs.html
			}

			for( var key in attrs)
				el.setAttribute(key, attrs[key])
		}

		if (childs)
			for(var i=0; i<childs.length; i++)
				el.appendChild(el[i])

		return el
	}

	public mapEl(els, attrName)
	{
		var map = {}
		for(var i=0; i<els.length; i++)
			map[ els[i].getAttribute(attrName)] = els[i]

		return map
	}

	public bind(events, el, callback, thisArg?, useCapture?) {
		thisArg && (callback = _.bind(callback, thisArg))
		events.split(' ').forEach( (evt)=> el.addEventListener(evt,callback,useCapture))
	}

	public $on(event , selector, callback, thisArg?) { return this.$el.on(event, selector, thisArg? _.bind(callback, thisArg) : callback) }

	public setOptions(el: HTMLSelectElement, options, keyVal?, keyText?)
	{
		while( el.options.length)
			el.remove(0)

		if (Array.isArray(options))
			options.forEach((opt)=> {
				el.add( this.create('option', { text: opt[keyText], value: opt[keyVal] }))
			})
		else
			for(var key in options)
				el.add( this.create('option', { text: options[key], value: key }))
	}


	// Childs
	// -----------------------------------------------------

	public addChild(view: View)
	{
		this.childs[view.id] = view

		if (this.options.tabs && view.id != this.get('active_child'))
			this.hideEl(view.el)
	}

	public removeChild(view: View)
	{
		this.stopListening(view)
		delete this.childs[view.id]
	}



	// Show / Hide / Remvoe
	// -----------------------------------------------------

	public isShown(): boolean
	{
		return this.get('shown')
	}

	public show(arg?)
	{
		if (this.isShown())
			return

		return when( this.onShow(arg), ()=> {
			this.showEl(this.el)
			this.set('shown', true)
			this.emit('show', this)
		}, this.handleReject)
	}

	public hide(opts?)
	{
		if (!this.isShown())
			return

		return when( this.onHide(), ()=> {
			this.hideEl(this.el)
			this.set('shown', false)
			this.emit('hide', this)
		}, this.handleReject)
	}

	public remove()
	{
		for(var key in this.childs)
			this.childs[key].remove()

		this.emit('remove')
		this.onRemove()

		delete VIEW_INSTANCES[this.id]
		delete this.bindings

		if (ACTIVE_VIEW==this)
			ACTIVE_VIEW = null

		if (this.el.parentNode)
			this.el.parentNode.removeChild(this.el)

		delete this.el
		super.free.call(this)
	}



	// Callbacks for Show / Hide / Remove
	// -----------------------------------------------------

	public onShow(arg?) { /* Override me */ }
	public onHide(arg?) { /* Override me */ }
	public onRemove() { /*Override me*/ }



	// Navigation
	// -----------------------------------------------------

	public navigate(path?: string, arg?)
	{
		if (!path)
			return this.navigation([], arg)

		NAVIGATION_ACTIVE = true
		var ret

		if (path[0]=='/') {
			var view = this
			var steps = []

			while (view.parent) {
				steps.push('..')
				view = view.parent
			}
			if (path.length>1)
				steps = steps.concat(path.substr(1).split('/'))

			// TODO normalize path
			ret = this.navigation(steps, arg)
		}
		else {
			ret = when( this.navigation( path.split('/'), arg), undefined, this.handleReject)
		}

		ret.ensure( ()=> {
			NAVIGATION_ACTIVE = false
		})

		return ret
	}

	/*protected*/ navigation(steps, arg?)
	{
		try {
			if (steps.length==0) {
				ACTIVE_VIEW = this

				return when( this.show(arg), ()=> {
					if (this.routes['/'])
						return this.routes['/'](arg)
					else
						return this
				}, this.handleReject)
			}

			var step = steps.shift()

			if (step=='..') {
				return when( this.hide(), ()=> this.parent.navigation(steps,arg), this.handleReject)
			}

			return when( this.show(), ()=> {
				if (this.routes[step])
					return this.routes[step].apply(this, steps.concat(arg))

				if (step in this.childs)
					return this.navigateToChild(step, steps, arg)
				else
					throw new Error("Unknown navigation step: "+step)
			}, this.handleReject)
		} catch (e) {
			this.throwError(e)
		}
	}

	private navigateToChild(name, steps, arg)
	{
		if (this.options.tabs) {
			var active = this.get('active_child')
			if (active && active != name) {
				return when( this.childs[active].hide(), ()=> {
					this.set('active_child', name)
					return this.childs[name].navigation(steps, arg)
				}, this.handleReject)
			}
		}

		this.set('active_child', name)
		return this.childs[name].navigation(steps, arg)
	}

	/*protected*/ bindNavigation()
	{
		this.$el.on( this.options.navsEvent, this.options.navsSelector, (e : Event)=> {
			e.preventDefault()
			e.stopPropagation()

			if (NAVIGATION_ACTIVE)
				return

			var src = <Element> e.target
			var nav = $(src).is(this.options.navsSelector) ? src : $(src).closest(this.options.navsSelector).get(0)
			var link = nav.getAttribute('href') || nav.getAttribute('data-navigate')

			if (link[0]=='#')
				link = link.substr(1)

			this.navigate(link)
			return false
		})
	}



	// Model
	// -----------------------------------------------------

	public setModel(name, model)
	{
		if (this[name])
			this.stopListening(this[name])

		this[name] = model

		var evt = 'change:'+name
		this.listenTo(model, 'change', ()=> this.emit(evt, model, false))
		this.emit(evt, model, true, this)
		return this
	}

	public refreshBindings()
	{
		if (!this.bindings)
			return

		var bindings = this.bindings
		  , item

		for(var id in this.bindings) {
			item = bindings[id]
			if ( item.view[ item.modelName ])
				this.onModelChanged(id, item.view[item.modelName], true)
		}
	}

	/*protected*/ onModelChanged(bindingsId, model, force?)
	{
		if (!this.bindings[bindingsId])
			return

		this.bindings[bindingsId].list.forEach( (def) => {
			if (! (force || model.isChanged(def.key)))
				return

			var val = model.get(def.key)
			  , el = def.el

			if (def.not)
				val = !val

			switch (def.type) {
				case 'attr':
					(val==false || val==null) ? el.removeAttribute(def.largs) : el.setAttribute(def.largs, val)
					break;

				case 'class':
					(val) ?	el.classList.add(def.largs) : el.classList.remove(def.largs)
					break;

				case 'enabled':
					(val) ? el.removeAttribute('disabled') : el.setAttribute('disabled','disabled')
					break;

				case 'html':
					el.innerHTML = val
					break;

				case 'show':
					if ( el == this.el)
						val ? this.show() : this.hide()
					else
						val ? this.showEl(el) : this.hideEl(el)
					break;

				case 'text':
					el.textContent = val
					break;

				case 'value':
					(<HTMLInputElement> el).value = val
					break;

				case 'selected':
					var sel = <HTMLSelectElement> el
					var i = _.findIndex( sel.options, (opt)=> opt.value==val)
					sel.selectedIndex = i
					break;

				case 'visible':
					(val) ? (el.style.visibility='visible') : (el.style.visibility='hidden')
					break;
			}
		})
	}



	// Form
	// -----------------------------------------------------
	public fillForm(form, model)
	{
		var els = form.elements
		  , isModel = model instanceof Model
		  , inp
		  , val

		for(var i=0, l=els.length; i<l; i++) {
			inp = els[i]
			val = isModel ? model.get(inp.name) : model[inp.name]

			if (inp.hasAttribute('data-format'))
				val = this.formats[inp.getAttribute('data-format')](val, 'write')

			$(inp).val( val != undefined ? val : '')
		}
	}

	public readForm(form, model = <any> {})
	{
		var els = form.elements
		  , isModel = model instanceof Model
		  , inp
		  , val

		for(var i=0, l=els.length; i<l; i++) {
			inp = els[i]

			if (!inp.name || inp.type=='button' || inp.type=="submit")
				continue

			val = $(inp).val()

			if (inp.hasAttribute('data-format'))
				val = this.formats[inp.getAttribute('data-format')](val, 'read')
			else if (inp.getAttribute('type')=='number')
				val = parseInt(val, 10)

			isModel ? model.set(inp.name, val) : model[inp.name]=val
		}

		return model
	}

	public fillFields(els, model, nameAttr = 'data-name')
	{
		var isModel = model instanceof Model
		  , el
		  , name
		  , val

		for(var i=0, l=els.length; i<l; i++) {
			el = els[i]
			name = el.getAttribute(nameAttr)
			val = isModel ? model.get(name) : model[name]

			if (el.hasAttribute('data-format'))
				val = this.formats[el.getAttribute('data-format')](val, 'write')

			el.textContent = val != undefined ? val : ''
		}
	}

	// Parsing
	// -----------------------------------------------------

	public parseDOM()
	{
		var queue = []

		if (this.el.hasAttribute('data-bind'))
			queue.push(this.el)

		var filter = (node)=>this.parseNode(node)
		var treeWalker = document.createTreeWalker(this.el, NodeFilter.SHOW_ELEMENT, <any> filter, false)

		var node
		while( (node = treeWalker.nextNode()) != null) {
			if (node.hasAttribute('data-bind'))
				queue.push(node)
		}

		queue.forEach( (el)=> {
			try {
				var bind = el.getAttribute('data-bind')
				this.addDataBind(el, bind)
			} catch (e) {
				var desc = el.tagName + (el.id ? '#'+el.id : '') + '[data-bind="'+el.getAttribute('data-bind')+'"]'
				var errorMsg = 'data-bind error at: ' + desc + ' message:'+e.toString()
				throw new Error(errorMsg)
			}
		})
	}

	private parseNode(node)
	{
		if (node.hasAttribute('data-parse')) {
			var data = node.getAttribute('data-parse')
			switch(data){
				case 'skip': return NodeFilter.FILTER_SKIP
				case 'no-parse': return NodeFilter.FILTER_REJECT
				default: throw new Error("invalid data-parse attribute: "+data)
			}
		}

		if (node['__XBONE_VIEW'] === true)
			return NodeFilter.FILTER_REJECT

		if (node.hasAttribute('data-view')) {
			this.parseDataView(node)
			return NodeFilter.FILTER_REJECT
		}

		return NodeFilter.FILTER_ACCEPT
	}

	private parseDataView(node, Class?)
	{
		if (VIEW_INSTANCES[node.id])
			return

		var ClassName, Class, options
		ClassName = node.getAttribute('data-view') || 'view'

		options = {
			el: node,
			parent: this,
		}

		if (!Class) {
			if ((Class = View.getClass(ClassName)) == null)
				throw new Error("Unknown class: "+ClassName)
		}

		var view = new Class(options)
		view.init()
	}

	private addDataBind(el, databind: string)
	{
		var bindings = databind.split(';')
		for(var i=0; i<bindings.length; i++) {
			var def = this.parseDataBind(bindings[i])

			if (BINDING_TYPES.indexOf(def.type) == -1)
				throw new Error('Unknown binding type: '+def.type)

			var view = View.get(def.view)

			if (!view)
				throw new Error('Unable to resolve view: '+def.view)

			if ( _.contains( EVENT_BINDING_TYPES, def.type)) {
				if (def.type=='value') {
					this._addValueDataBind(el, (def.largs || 'change'), view, def.model, def.key)
				}
				else if (def.type=='selected') {
					this._addSelectedDataBind(el, (def.largs || 'change'), view, def.model, def.key)
				}
				else {
					this._addEventDataBind(el, (def.largs), view, def.model, def.key, (def.rargs ? def.rargs.split(',') : []))
				}
			}

			if ( _.contains( DATA_BINDING_TYPES, def.type)) {
				var id = (view == this) ? def.model : view.id+':'+def.model
				if (! (id in this.bindings)) {
					this.bindings[id] = {view: view, modelName: def.model, list: []}
					this.listenTo(view, 'change:'+def.model, (model,force)=> this.onModelChanged( id, model, force))
				}

				def.el = el
				this.bindings[id].list.push(def)
			}
		}
	}

	private _addEventDataBind(el, events, view, modelName, key, args)
	{
		$(el).on(events, (e: Event) => {
			var target = view[modelName]
			if (typeof(target[key])=='function')
				target[key].apply(target, args.concat([e]))
			else
				throw new TypeError('Not callable: '+view.id+':'+modelName+':'+key)
		})
	}

	private _addValueDataBind(el, events, view, modelName, key)
	{
		$(el).on(events, (e: Event) => {
			view[modelName].set(key, el.value)
		})
	}

	private _addSelectedDataBind(el, events, view, modelName, key)
	{
		$(el).on(events, (e: Event) => {
			view[modelName].set(key, el.options[el.selectedIndex].value)
		})
	}

	private parseDataBind(databind: string)
	{
		var match = databind.match(/^\s*(\w+)(?:\((.*?)\))?\s*=\s*(!)?(\w*[:]?[\w$]*[:]?@?[\w$.]+)(?:\((.*?)\))?\s*$/)
		if (match==null)
			throw new Error("Invalid data-bind format")

		var parts = match[4].split(':')
		while(parts.length<3) parts.unshift(null);

		return {
			el:    null,
			type:  match[1],
			largs: match[2],
			not:   match[3]=='!',
			view:  parts[0] || this.id,
			model: parts[1] || 'self',
			key:   parts[2],
			rargs: match[5],
		}
	}


	// Static
	// -----------------------------------------------------

	static getActive() : View
	{
		return ACTIVE_VIEW
	}

	static get(key) : View
	{
		return VIEW_INSTANCES[key]
	}

	static register(name, Class, replace?)
	{
		if (VIEW_CLASSES[name] && !replace)
			throw new Error("Class with same name exits: "+name)

		VIEW_CLASSES[name] = Class
	}

	static getClass(name)
	{
		return VIEW_CLASSES[name]
	}



	// Utils
	// ------------------------------------------------------
	private parseDataAttr(content: string)
	{
		return (new Function('return ' + content + ';' )())
	}

}

View.register('view', View)

