import Model = require('../model')

export = DeepModel

class DeepModel extends Model {

	public get(key: string, opts?) {
		if (key[0]=='@')
			return this[key.substr(1)]

		var parts = key.split('.')
		  , attrs = this.attrs

		for(var i=0; i<parts.length; i++) {
			if (attrs)
				attrs = attrs[parts[i]]
			else
				return undefined
		}

		return attrs
	}

	/*protected*/ _set(key, value)
	{
		if (key[0]=='@') {
			key = key.substr(1)
			if (this[key] != value) {
			 	this[key] = value
			 	return true
			}
		}
		else {
			var parts = key.split('.')
			  , len = parts.length -1
			  , attrs = this.attrs
			  , i, k

			for(i=0; i<len; i++)
				attrs = attrs[parts[i]]

			k = parts[len]
			if (attrs[k] != value) {
				attrs[k] = value
				return true
			}

		}
	}

	public has(key: string) {
		if (key[0]=='@')
			return (key.substring(1) in this)

		var parts = key.split('.')
		  , len = parts.length -2
		  , attrs = this.attrs
		  , i, k

		for(i=0; i<len; i++) {
			k = parts[i]
			if ( typeof attrs[k] != 'object')
				return false

			attrs = attrs[k]
		}

		return parts[len] in attrs
	}

	public isChanged(key: string) {
		if (!this.changes)
			return false

		if (this.changed[key])
			return true

		for(var i=0; i<this.changes.length; i++) {
			var ckey = this.changes[i]
			if (DeepModel.isSubKey(key, ckey) || DeepModel.isSubKey(ckey, key)) {
				this.changes[key] = true
				return true
			}
		}

		return false
	}

	static isSubKey(sub: string, key: string)
	{
		return (sub.length >= key.length  &&  sub.substr(0,key.length)==key)
	}

	static extend(dest)
	{
		var proto = DeepModel.prototype;
		['get','_set','has','isChanged'].forEach( (fn)=> { dest.prototype[fn] = proto[fn] })
	}

}
