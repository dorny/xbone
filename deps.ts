/// <reference path="./deps/zepto.d.ts"/>

/// <amd-dependency path="./deps/lodash"/>
/// <amd-dependency path="./deps/when"/>
/// <amd-dependency path="./deps/zepto"/>

declare var require;

export var lodash = require('./deps/lodash')
export var when = require('./deps/when')
export var zepto : ZeptoStatic = $


