import deps = require('./deps')
import Events = require('./events')
var _ = deps.lodash
var when = deps.when


export = Model


class Model extends Events {

	// Acces to static properties in child classes with "this.constructor"
	public constructor;

	// Main attriubtes
	public attrs;

	// Usage in change events
	public changes: string[];
	public changed: {[key: string]: boolean};

	// Triggers for computed propterties
	static triggers : {[attribute: string]: string[]};

	constructor(attrs = {})
	{
		super()
		this.attrs = attrs
	}

	public init(opts?)
	{

	}

	public get(key: string, opts?) {
		if (key[0]=='@')
			return this[key.substr(1)]
		else
			return this.attrs[key]
	}

	public set(key, value?, opts?) {

		var changes = []

		if (typeof key=='object') {
			var replace = value
			value = key

			if (replace) {
				if (this.attrs)
					changes = changes.concat(Object.keys(this.attrs))

				if (value)
					changes = changes.concat(Object.keys(value))

				changes = _.uniq(changes)
				this.attrs = key
			} else {
				for (var key in value)
					if (this._set(key,value[key]))
						changes.push(key)
			}
		}
		else {
			if (this._set(key,value))
				changes.push(key)
		}

		if (changes.length && !(opts && opts.silent))
			this.emitChange(changes)
	}

	/*protected*/ _set(key, value)
	{
		if (key[0]=='@') {
			key = key.substr(1)
			if (this[key] != value) {
			 	this[key] = value
			 	return true
			}
		}
		else {
			if (this.attrs[key] != value) {
				this.attrs[key] = value
				return true
			}
		}
	}

	public has(key: string) {
		return (key in this.attrs)
	}

	public onChange(callback) {
		this.on('change', callback)
	}

	public emitChange(keys: string[]) {
		var changes = this.changes = keys
		var changed = this.changed = {}

		for(var i=0; i<keys.length; i++)
			changed[keys[i]] = true

		if (this.constructor.triggers) {
			var triggers = this.constructor.triggers
			keys.forEach( (key)=> {
				if (key in triggers) {
					var list =triggers[key]
					for(var i=0; i<list.length; i++) {
						var x = list[i]
						if (!changed[x]) {
							changed[list[i]] = true
							changes.push(x)
						}
					}
				}
			})
		}

		this.emit('change', changes, this)
		this.changes = null
		this.changed = null
	}

	public isChanged(key: string) {
		return this.changed[key]
	}

	public free() {
		delete this.attrs
		this.off()
		this.stopListening()
	}


	public async(callback, delay = 0, thisArg?) { return setTimeout( thisArg? _.bind(callback, thisArg) : callback, delay) }
	public defer(callback, delay = 0, thisArg?) {
		var defer = when.defer()
		setTimeout( ()=> {
			try {
				defer.resolve( thisArg? _.bind(callback, thisArg) : callback )
			} catch(e) {
				defer.reject(e)
			}
		})
		return defer.promise
	}

	public throwError(error)
	{
		setTimeout( ()=> { throw error })
	}

	public handleReject(reason)
	{
		if (reason instanceof Error)
			setTimeout( ()=> { throw reason })

		throw reason
	}

}
